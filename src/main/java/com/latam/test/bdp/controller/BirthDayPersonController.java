package com.latam.test.bdp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.latam.test.bdp.dto.person.BirthDayDataDTO;
import com.latam.test.bdp.dto.person.PersonDTO;
import com.latam.test.bdp.dto.response.ResponseDTO;
import com.latam.test.bdp.dto.response.StatusDTO;
import com.latam.test.bdp.service.IPersonBirthDayService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/api")
@Slf4j
public class BirthDayPersonController {
	
	@Autowired
	private IPersonBirthDayService personService;
	
	@PostMapping(value = "/person", produces = "application/json")
	public ResponseEntity<String> newPerson(@Valid @RequestBody PersonDTO personDto) {
		log.info("Controller BirthDay date : " + personDto.getBirthDay());		
		BirthDayDataDTO birthDayDataDTO = this.personService.getBirthDayData(personDto.getBirthDay());		
		return new ResponseEntity<>(  new Gson().toJson(this.setResponse(personDto, birthDayDataDTO, HttpStatus.OK, "OK")) , HttpStatus.OK);
	}
	
	private StatusDTO setResponse(PersonDTO personDto, BirthDayDataDTO birthDayDataDTO, HttpStatus httpStatus, String message) {
		
		ResponseDTO response = new ResponseDTO();
		response.setFirstName(personDto.getFirstName());
		response.setFirstSurName(personDto.getFirstSurName());
		response.setBirthDayData(birthDayDataDTO);
		
		StatusDTO statusDTO = new StatusDTO();
		statusDTO.setCode(httpStatus.toString());
		statusDTO.setMessage(message);
		statusDTO.setResponse(response);
		
		
		return statusDTO;
	}
	
	

}
