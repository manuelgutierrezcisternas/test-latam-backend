package com.latam.test.bdp.config;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.latam.test.constans.Constant;
import com.latam.test.util.PoemException;

@Component
public class RestFulWebServiceClient {

	@Autowired
	private RestTemplate restTemplate;
	
	public List<LinkedHashMap<String, String>> getPoems(HttpMethod httpMethod, String url) throws PoemException{
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, Constant.APPLICATION_JSON);
		HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<List> poems = null;
		
		try {
			
			poems = this.restTemplate.exchange(url, httpMethod, requestEntity, List.class);
			
			if (poems.getStatusCode() != HttpStatus.OK) {
				throw new PoemException(Constant.ERROR_POEM_SERVICE);
			}
			
		} catch (Exception e) {
			throw new PoemException(Constant.ERROR_POEM_SERVICE);
		}
		
		return poems.getBody();
	}
}
