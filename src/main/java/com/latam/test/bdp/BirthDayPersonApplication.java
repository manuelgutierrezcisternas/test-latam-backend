package com.latam.test.bdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BirthDayPersonApplication {

	public static void main(String[] args) {
		SpringApplication.run(BirthDayPersonApplication.class, args);
	}
	

}
