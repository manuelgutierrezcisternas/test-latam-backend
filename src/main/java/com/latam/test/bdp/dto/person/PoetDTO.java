package com.latam.test.bdp.dto.person;

import java.io.Serializable;

import lombok.Data;

@Data
public class PoetDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9023858993128607678L;
	private String name;
	private String url;
}
