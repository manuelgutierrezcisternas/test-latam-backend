package com.latam.test.bdp.dto.person;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.latam.test.constans.Constant;

import lombok.Data;

@Data
public class PersonDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6108334194127719857L;
	
	@NotNull(message = Constant.FIELD_IS_REQUIRED)
	@NotBlank(message = Constant.FIELD_IS_REQUIRED )
	private String firstName;
	private String middleName;
	@NotNull(message = Constant.FIELD_IS_REQUIRED)
	@NotBlank(message = Constant.FIELD_IS_REQUIRED )
	private String firstSurName;
	private String secondSurName;
	@NotNull(message = Constant.FIELD_IS_REQUIRED)
	@NotBlank(message = Constant.FIELD_IS_REQUIRED )
	private String birthDay;
	
}
