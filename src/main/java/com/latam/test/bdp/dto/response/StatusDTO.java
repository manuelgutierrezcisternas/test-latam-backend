package com.latam.test.bdp.dto.response;

import java.io.Serializable;

import lombok.Data;

@Data
public class StatusDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3006971592747728463L;
	private String code;
	private String message;
	private ResponseDTO response;
}
