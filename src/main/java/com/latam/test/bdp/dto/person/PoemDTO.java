package com.latam.test.bdp.dto.person;

import java.io.Serializable;

import lombok.Data;

@Data
public class PoemDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8573779532492110163L;
	private String title;
	private String content;
	private String url;
	private PoetDTO poet;
	
}
