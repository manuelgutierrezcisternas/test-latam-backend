package com.latam.test.bdp.dto.person;

import java.io.Serializable;

import lombok.Data;

@Data
public class BirthDayDataDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7853887115136801217L;
	
	private Integer age;
	private long days4BirthDay;
	private PoemDTO poem;
	private boolean isBirthDay;

}
