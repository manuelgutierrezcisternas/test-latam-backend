package com.latam.test.bdp.dto.response;

import java.io.Serializable;

import com.latam.test.bdp.dto.person.BirthDayDataDTO;

import lombok.Data;

@Data
public class ResponseDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1329258129004866004L;
	private String firstName;
	private String firstSurName;
	private BirthDayDataDTO birthDayData;

}
