package com.latam.test.bdp.service;

import com.latam.test.bdp.dto.person.BirthDayDataDTO;

public interface IPersonBirthDayService {
	
	BirthDayDataDTO getBirthDayData(String birthDate);

}
