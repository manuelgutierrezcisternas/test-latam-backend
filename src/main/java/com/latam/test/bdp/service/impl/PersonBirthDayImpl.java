package com.latam.test.bdp.service.impl;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.latam.test.bdp.config.RestFulWebServiceClient;
import com.latam.test.bdp.dto.person.BirthDayDataDTO;
import com.latam.test.bdp.dto.person.PoemDTO;
import com.latam.test.bdp.service.IPersonBirthDayService;
import com.latam.test.constans.Constant;
import com.latam.test.util.PoemException;
import com.latam.test.util.Util;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PersonBirthDayImpl implements IPersonBirthDayService { 

	@Autowired
	private RestFulWebServiceClient client;
	
	
	
	public BirthDayDataDTO getBirthDayData(String birthDate) {
		
		BirthDayDataDTO birthDayDataDTO = this.days4BirthDay(birthDate);
		
		birthDayDataDTO.setBirthDay(Util.validateBirthDay(birthDayDataDTO.getDays4BirthDay()));
		birthDayDataDTO.setPoem(null);
		if (birthDayDataDTO.isBirthDay()) {
			setPoemToBirthDayData(birthDayDataDTO);
		}
		
		return birthDayDataDTO;
		
	}

	private void setPoemToBirthDayData(BirthDayDataDTO birthDayDataDTO) {
		int maxPoems=0;
		try {
			List<LinkedHashMap<String, String>> poems = this.client.getPoems(HttpMethod.GET, Constant.POEM_URL);
			maxPoems = poems.size();
			LinkedHashMap<String , String> poem = poems.get(  Util.randomNumber(maxPoems)  );
			PoemDTO p = new PoemDTO();
			p.setContent( poem.get("content") );
			p.setTitle(poem.get("title"));
			p.setUrl(poem.get("url"));
			birthDayDataDTO.setPoem( p );

		} catch (PoemException e) {
			
			PoemDTO p = new PoemDTO();
			p.setContent( e.getMessage() );
			birthDayDataDTO.setPoem( p );			
			log.error(e.getMessage());
		}
	}
	
	
	
	private BirthDayDataDTO days4BirthDay(String birthDate) {
		
		long totalDias = 0L;
		BirthDayDataDTO birthDayDataDTO = new BirthDayDataDTO();
		Period p = null;
		try {
            
            LocalDate today = LocalDate.now();

           
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constant.FORMAT_DATE_BIRTHDAY);
            LocalDate birthday = LocalDate.parse(birthDate, formatter);
            LocalDate nextBDay = birthday.withYear(today.getYear());

           
            if (birthday.isAfter(today) || birthday.isEqual(today)) {
                nextBDay = nextBDay.plusYears(1);
            }

           
            p = Period.between(birthday, today);
            totalDias = ChronoUnit.DAYS.between(nextBDay, today);
            
            if (totalDias < 0) { totalDias = totalDias * -1; }
            
            if (totalDias == 366) {
               totalDias = 0;
              
            } 

        }catch (DateTimeParseException e) {
           log.error(e.getMessage(), e);
        }     
		
    	birthDayDataDTO.setAge(p == null ? 0 :p.getYears() );
    	birthDayDataDTO.setDays4BirthDay(totalDias);
		
		return birthDayDataDTO;
		
	}
	
	
}
