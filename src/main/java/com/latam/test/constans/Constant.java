package com.latam.test.constans;

public class Constant {
	
	public static final String POEM_URL = "https://www.poemist.com/api/v1/randompoems";
	public static final String FORMAT_DATE_BIRTHDAY =  "yyyy-MM-dd";
	public static final String APPLICATION_JSON =  "application/json";
	public static final String ERROR_POEM_SERVICE = "Error : Poem is not available. Please see the log for more information";
	public static final String FIELD_IS_REQUIRED = "Field is required";

}
