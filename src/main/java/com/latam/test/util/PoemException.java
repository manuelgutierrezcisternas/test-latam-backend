package com.latam.test.util;

public class PoemException extends Exception{

	/** 
	 * 
	 */
	private static final long serialVersionUID = -1422999173407200339L;
	
	public PoemException(String message) {
		super(message);
	}
	

}
