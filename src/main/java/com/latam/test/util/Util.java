package com.latam.test.util;

import java.util.Random;

/**
 * Class for system utilities
 * @author Manuel Gutierrez
 *
 */

public class Util {
	

	/**
	 * 
	 * @param max
	 * @return
	 */
	public static  int randomNumber(int max) {
		
		Random rand = new Random(); 
		return rand.nextInt(max); 
	}
	
	
	/**
	 * @param days4BirthDay
	 * @return
	 */
	public static Boolean validateBirthDay(long days4BirthDay) {
		return days4BirthDay == 0L ? Boolean.TRUE : Boolean.FALSE;
		
	}
	
	
	

}
