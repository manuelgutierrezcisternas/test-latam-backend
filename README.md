Datos para correr el test

Backend
========
Para el desarrollo de la prueba, se usaron las siguentes librerias y componentes:

- Eclipse 2019-06 (4.12.0)
- JDK 1.8.0_161
- Spring Boot
- Gson, para el trabajo con JSON
- Maven para las dependencias de librerias

La aplicacion se levanta ejecutando la clase 
BirthDayPersonApplication, para las pruebas se uso POSTMAN como cliente REST

1) El endpoint sin autenticar de la aplicación es el:

- http://localhost:8080/api/person
 metodo POST
 
Estos son los JSON que se usaron durante las pruebas:
 
 1) {"firstName":"Manuel","firstSurName":"Gutierrez","birthDay":"1977-02-24"}
 2) {"firstName":"Manuel","middleName":"Alfredo","firstSurName":"Gutierrez","secondSurName":"Cisternas","birthDay":"1977-02-24"}
 

Para ejecutar el test se deben levantar los dos proyectos , estar conectado a internet e ingresar los datos requeridos en el formulario.